#!/usr/bin/python3
#
from pylab import *
from numpy import *

hc = 1.986e-10 #uJ nm

def SSH_template(lambda_nm, lambda_max = 330): #From Stavenga (2010) assuming one pigment at 330nm
    a0 = 380
    a1 = 6.09
    xa = log10(lambda_nm/lambda_max)
    return exp( -a0*xa**2 * (1 + a1*xa + a1**2 * xa**2 *3/8) )

if __name__ == "__main__":
    figure(1)
    lambda_a = linspace(290,400,100)
    plot(lambda_a,SSH_330(lambda_a),'k')
    show()
