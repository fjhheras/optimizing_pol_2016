Estimation of the absoption profile of DRA R7/8 of very blue sky. 

* absorptionDRA.py: Main file. Plots absorption vs exponential absorption and fits a polynomial to the factor between those
* nomograms.py: Contains absorption spectrum of 330 rhodopsin with only one pigment
* read_skylight: It takes data from csv files and produce absolute spectral irradiance for blue sylight

