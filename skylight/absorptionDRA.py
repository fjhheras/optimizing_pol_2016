#!/usr/bin/python3
# Plots absorption vs exponential absorption and fits a polynomial to the factor between those
#
# Figure 1: Dashed line is spectral photon flux, and continuous line is absorbed spectral photon flux (Figure 2 in the text)
# Figure 2: Rate of absorbed photons vs length for various approximations (Figure 3 in the text)
# Figure 3: Square error of the cuadratic and linear approximations
#
# Author: fjhheras@gmail.com

from pylab import *
from numpy import *
from scipy import integrate
import nomograms
import read_skylight

plot_lambda_extended = False

Skylight = read_skylight.generate_skylight_pretoria()
k_rel = lambda x : nomograms.SSH_template(x, lambda_max = 335)
F_ratio = 2
area_R7 = 1.9 #um2
d = sqrt(1.9/pi)*2 #um
print("effective R7 diameter = ", d)

k_0 = 0.0075
l = logspace(0,3)

lambda1=300 # nm
lambda2=335*1.231

factor_optics = (pi/4 /F_ratio *d)**2
accesible_photon_rate_f = lambda x : factor_optics * Skylight(x)
N = integrate.quad(accesible_photon_rate_f,lambda1,lambda2)[0]
print("Accesible photon flux between ", lambda1, " and ", lambda2, " is ", N, " ph/s")

def photons_absorbed (kl,plot_spectrum = False,lambda1=lambda1,lambda2=lambda2):
    #print("Processing kl=",kl)
    photon_rate_f = lambda x : factor_optics * Skylight(x) * (1-exp(-k_rel(x)*kl))
    if plot_spectrum:
        lambda_a = linspace(lambda1,lambda2)
        plot(lambda_a,photon_rate_f(lambda_a),'k')
        plot(lambda_a,accesible_photon_rate_f(lambda_a),'k--')
    return integrate.quad(photon_rate_f,lambda1,lambda2)[0] #photons s-1

photons_absorbed_v = vectorize(photons_absorbed)

figure(1)
photons_absorbed(100*k_0,plot_spectrum=True)
if plot_lambda_extended:
    lambda_extended = linspace(lambda1,600)
    plot(lambda_extended,accesible_photon_rate_f(lambda_extended),'k:')
xlabel("Wavelength (nm)")
ylabel("Spectral photon flux (ph/s/nm)")


pha = photons_absorbed_v(l*k_0)  #Exact calculation
pha_exp = N*(1-exp(-k_0*l))      #Exponential absorption

h=0.35
m=0.96
pha_warrant = N*(k_0*l)**m / (10**(h*m) + (k_0*l)**m) #Warrant and Nilsson approximation

a = polyfit(k_0*l,pha/pha_exp,2) #Fitting a quadratic function multiplied by a exponential
b = polyfit(k_0*l,pha/pha_exp,1) #Fitting a linear function multiplied by a exponential

figure(2)
loglog(l,pha,'k')
loglog(l,pha_exp,'k--')
loglog(l,pha_warrant,'b--')
loglog(l,pha_exp*(a[2]+a[1]*(k_0*l)+a[0]*(k_0*l)**2),'r--')
loglog(l,pha_exp*(b[1]+b[0]*(k_0*l)),'r:')
ylabel("Absorbed photon rate")
xlabel("Photoreceptor length (um)")

figure(4)
semilogx(l, (pha - pha_exp*(a[2]+a[1]*(k_0*l)+a[0]*(k_0*l)**2))/pha,'r--')
semilogx(l, (pha - pha_exp*(b[1]+b[0]*(k_0*l)))/pha,'r:')
ylabel("Relative error of the quadratic-exp approximation")

show()
