#!/usr/bin/python3
# Reads csv tables for very blue skylight at Pretoria, KOK (1972)
# The function generate_skylight_retoria returns quantal spectral wavelength in photons/s/um2/nm
#
# Author: fjhheras@gmail.com
import csv
from pylab import *
from numpy import *
from scipy.interpolate import interp1d

hc = 1.986e-10 #uJ nm

def read_cvs_lin4(file_name):
    as_file = open(file_name,'r',newline='')
    data = csv.reader(as_file)
    next(data)
    x=[]
    y1=[]
    y2=[]
    y3=[]
    for line in data:
        [w,daylight,sunlight,skylight] = line
        x.append(float(w))
        y1.append(float(daylight))
        y2.append(float(sunlight))
        y3.append(float(skylight))
    del data
    as_file.close()
    del as_file
    print(x)
    return [array(x),array(y1),array(y2),array(y3)]

def generate_skylight_pretoria(return_auxiliary = False, photons = True):
    [lambda_s, data_daylight,data_sunlight,data_skylight] = read_cvs_lin4("pretoria.csv")

    skylight_raw = interp1d(lambda_s,data_skylight*1e-9) #Data in uW/cm2/10nm ----> uW/um2/nm
    skylight_ = lambda x: skylight_raw(x)/(hc/x) #uW/um2/nm ---> ph/um2/nm

    if photons:
        if return_auxiliary:
            daylight_raw = interp1d(lambda_s,data_daylight*1e-9) #Data in uW/cm2/10nm ----> uW/um2/nm
            daylight_ = lambda x: daylight_raw(x)/(hc/x)
            return vectorize(skylight_), vectorize(daylight_)
        else:
            return vectorize(skylight_)
    else:
        if return_auxiliary:
            daylight_raw = interp1d(lambda_s,data_daylight*1e-9) #Data in uW/cm2/10nm ----> uW/um2/nm
            return vectorize(skylight_raw), vectorize(daylight_raw) 
        else:
            return vectorize(skylight_raw) 


if __name__ == "__main__":
    skylight_pretoria, daylight_pretoria = generate_skylight_pretoria(return_auxiliary = True, photons = False)
    figure(1)
    lambda_a = linspace(310,640,500)
    plot(lambda_a,skylight_pretoria(lambda_a),'b')
    plot(lambda_a,daylight_pretoria(lambda_a),'k')
    show()
