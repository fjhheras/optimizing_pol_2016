function maxi = maxi_polynomial(v)
%% Interpolates a second order polynomial around the index of the maximum in v and calculates the maximum
    [pippo,maxI] = max(v);
    if maxI == 1 || maxI == length(v) %If maximum is in the extreme, we do not correct.
        maxi = maxI
    else
        x=[maxI-1,maxI, maxI+1];
        y=v(x);
        yk = y./[(x(1)-x(2))*(x(1)-x(3)),(x(2)-x(1))*(x(2)-x(3)),(x(3)-x(1))*(x(3)-x(2))];
        maxi = (yk*(sum(x)-x)')/2/sum(yk);
    end
end