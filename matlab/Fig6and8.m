% Calculates how the number of discriminable polarization angles,
% the mutual information between opponent signal and pol angle,
% and the opponent signalling range DeltaQ changes
% when changing the relative lengths of R7 and R8, while keeping 
% the total rhabdom length constant
%
% Providing the variable "option" equals 3:
% Figure(1) -> Fig 8 a/b (depending on "set_values.m"), c, d
% Figure(2) -> Extra figure, like a/b but with mutual information
% Figure(3) -> Fig 6
%
% Author: fjh31@cam.ac.uk

l=100; % total length of the rhabdomere
option = 3; % 1 number discriminable angles, 2 mutual information, 3 both
step=1 % length increments in um

colour_plot=['b','r','g','k']
photon_flux=[1e5,3e5,1e6,3e6]
N_light_levels = size(photon_flux, 2)

maxi_nosat_a=[]
maxi_sat_a=[]


for i=1:N_light_levels

    performance_sat=[]; 
    performance_nosat=[]; 
    DeltaQ_a=[];
    DeltaQ_nosat_a=[];
    
    for l8=1:step:(l-1)
       if (option==1 || option==3)
          [sat ,nosat, DeltaQ, DeltaQ_no_sat] = n_discrim_angles(l-l8,l8,photon_flux(i));
       else
          [sat ,nosat, DeltaQ, DeltaQ_no_sat] = distanceMI(l-l8,l8,photon_flux(i));
       end

       DeltaQ_a = [DeltaQ_a, DeltaQ];
       DeltaQ_nosat_a = [DeltaQ_nosat_a, DeltaQ_no_sat];
       performance_sat=[performance_sat, sat];
       performance_nosat=[performance_nosat, nosat];

    end

    figure(1)
    subplot(1,2,1) 
    plot((1:step:(l-1))/l,performance_sat,[colour_plot(i),'--'])
    hold on
    plot((1:step:(l-1))/l,performance_nosat,colour_plot(i))
    
    maxi_sat      = maxi_polynomial(performance_sat); %interpolate for smoother maximum
    maxi_nosat    = maxi_polynomial(performance_nosat);
    maxi_nosat_a=[maxi_nosat_a, (maxi_nosat-1)*step+1];
    maxi_sat_a=[maxi_sat_a, (maxi_sat-1)*step+1];
    

    subplot(2,2,2) % optimal \hat{l}_8 vs light level
    
    semilogx(photon_flux(i),maxi_nosat_a(i)/l,[colour_plot(i),'+']);
    hold on
    semilogx(photon_flux(i),maxi_sat_a(i)/l,[colour_plot(i),'+']);

    figure(3)
    plot((1:step:(l-1))/100, DeltaQ_nosat_a,colour_plot(i))
    hold on;
    plot((1:step:(l-1))/100, DeltaQ_a,[colour_plot(i),'--'])

    

end

figure(1)
subplot(1,2,1)
xlabel('R8 length fraction')
if (option==1 || option==3)
    ylabel('Number of discriminable angles')
else
    ylabel('Mutual information (bits / integration time)')
end
subplot(2,2,2)
semilogx(photon_flux,maxi_nosat_a./l,'k')
semilogx(photon_flux,maxi_sat_a./l,'--k')

figure(3)
xlabel('R8 length fraction')
ylabel('Opponent signalling range')

if option==3
    maxi_nosat_a=[];
    maxi_sat_a=[];
    for i=1:N_light_levels

        performance_sat=[];
        performance_nosat=[];
        for l8=1:step:(l-1)
           [sat ,nosat, DeltaQ, DeltaQ_no_sat] = distanceMI(l-l8,l8,photon_flux(i));

           performance_sat=[performance_sat, sat];
           performance_nosat=[performance_nosat, nosat];

        end

        figure(2)
        plot((1:step:(l-1))/l,performance_nosat,colour_plot(i))
        hold on
        plot((1:step:(l-1))/l,performance_sat,[colour_plot(i),'--'])
        
        maxi_sat      = maxi_polynomial(performance_sat);
        maxi_nosat    = maxi_polynomial(performance_nosat);
        maxi_nosat_a=[maxi_nosat_a, (maxi_nosat-1)*step+1];
        maxi_sat_a=[maxi_sat_a, (maxi_sat-1)*step+1];

        figure(1)
        subplot(2,2,4)

        semilogx(photon_flux(i),maxi_nosat_a(i)/l,[colour_plot(i),'+'])
        hold on
        semilogx(photon_flux(i),maxi_sat_a(i)/l,[colour_plot(i),'+'])

    end

figure(1)
subplot(2,2,4)
semilogx(photon_flux,maxi_nosat_a./l,'k')
semilogx(photon_flux,maxi_sat_a./l,'--k') 
xlabel('R8 length fraction')

figure(2)
ylabel('Mutual information (bits / integration time)')
xlabel('R8 length fraction')

end



