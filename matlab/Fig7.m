% Calculates how discriminability depends on the polarization angle
% for two configurations l7=l8=50 um, and l7=90, l8=10
% It considers saturation if this variable
clear;
account_for_saturation = 0;
% is set to 1. If it is set to 0, it does not consider saturation
% Author: fjh31@cam.ac.uk

close all;
set_values;


N_cases = 3;
N_array=[1e4,1e5,1e6];

l7=50; l8=50;
N_colour=['b','r','g'];


for N_i=1:N_cases

    N = N_array(N_i)
    
    %% GENERATE PHOTON CATCH PROFILE FOR ALL c AND angle
    angle_0=0; steps = 200; % polarization angle steps
    angle_1=pi/2;
    angle_i_background = steps/2;
    anglearray = linspace(angle_0, angle_1, steps+1)*180/pi;
    [ L7s,L8s,L7svar,L8svar,L7,L8 ,L7M,L8M] = function_polarisation_stuff(N,c,l7,l8,k,D7,D8,deadtime,tau,angle_0,angle_1,steps);
    %% CALCULATE DISTANkCES BETWEEN A CERTAIN POINT angle_i_1, c_i_1 AND OTHERS
    if (account_for_saturation == 1)
        L7 = L7s;
        L8 = L8s;
        L8var = L8svar;
        L7var = L7svar;
    else
        L7var = L7;
        L8var = L8;
    end

    k_R7 = L7(angle_i_background);
    k_R8 = L8(angle_i_background);
        
    for anglei = 1:(steps)
        Delta_q_R7 = ( L7(anglei + 1) - L7(anglei) )./ k_R7; 
        Delta_q_R8 = ( L8(anglei + 1) - L8(anglei) ) ./ k_R8;
        err = L7var(anglei) ./ k_R7./ k_R7 + L8var(anglei) ./ k_R8 ./ k_R8 + internal_noise_l7 + internal_noise_l8;
        distance(anglei) = abs(Delta_q_R7-Delta_q_R8)./sqrt(err);
    end

    %% PLOT
    plot(anglearray(1:steps),distance/(anglearray(2)-anglearray(1)),N_colour(N_i),'Linewidth',2)
    hold on
end

l7=90; l8=10;

for N_i=1:N_cases

    N = N_array(N_i)
    
    %% GENERATE PHOTON CATCH PROFILE FOR ALL c AND angle
    angle_0=0; steps = 200; % polarization angle steps
    angle_1=pi/2;
    angle_i_background = steps/2;
    anglearray = linspace(angle_0, angle_1, steps+1)*180/pi;
    [ L7s,L8s,L7svar,L8svar,L7,L8,L7M,L8M ] = function_polarisation_stuff(N,c,l7,l8,k,D7,D8,deadtime,tau,angle_0,angle_1,steps);

    %% CALCULATE DISTANkCES BETWEEN A CERTAIN POINT angle_i_1, c_i_1 AND OTHERS
    if (account_for_saturation == 1)
        L7 = L7s;
        L8 = L8s;
        L8var = L8svar;
        L7var = L7svar;
    else
        L7var = L7;
        L8var = L8;
    end

    k_R7 = L7(angle_i_background);
    k_R8 = L8(angle_i_background);
        
    for anglei = 1:(steps)
        Delta_q_R7 = ( L7(anglei + 1) - L7(anglei) )./ k_R7; 
        Delta_q_R8 = ( L8(anglei + 1) - L8(anglei) ) ./ k_R8;
        err = L7var(anglei) ./ k_R7./ k_R7 + L8var(anglei) ./ k_R8 ./ k_R8 + internal_noise_l7 + internal_noise_l8;
        distance(anglei) = abs(Delta_q_R7-Delta_q_R8)./sqrt(err);
    end

    %% PLOT
    plot(anglearray(1:steps),distance/(anglearray(2)-anglearray(1)),[N_colour(N_i),'--'],'Linewidth',2)
    hold on
end

xlim([0 90])
ylim([0,0.16])
xlabel('Polarization angle (deg)')
ylabel('Discriminability (deg^{-1})')
