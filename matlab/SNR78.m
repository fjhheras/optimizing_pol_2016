function [S7,S8,SNR7,SNR8] = SNR78(l7,l8,N,saturation)
% Calculates SNR in R7 and R8 of length l7 and l8
% when incoming photon rate is N. Saturation is considered
% if "saturation==1", and not if "saturation==0"
% It calls "function_polarisation_stuff" to calculate the absorption 
% rates in R7 and R8 at 0 and pi/2 angle of polarisation. 

set_values;

steps=2;
angle_0=0; angle_1=pi/2;
[ L7s,L8s,L7svar,L8svar,L7,L8,L7M,L8M ] = function_polarisation_stuff(N,c,l7,l8,k,D7,D8,deadtime,tau,angle_0,angle_1,steps)

if saturation == 1
 %% Saturated response
 k_R7s = L7s(steps*1/2+1)
 k_R8s = L8s(steps*1/2+1)
 SNR7 = abs(L7s(1)-L7s(steps+1))/sqrt(L7svar(steps*1/2+1))
 SNR8 = abs(L8s(1)-L8s(steps+1))/sqrt(L8svar(steps*1/2+1))
 S7 = k_R7s
 S8 = k_R8s
else
% %% Non-saturated response
 k_R7 = L7M(steps*1/2+1)
 k_R8 = L8M(steps*1/2+1)
 SNR7 = abs(L7M(1)-L7M(steps+1))/sqrt(L7M(steps*1/2+1))
 SNR8 = abs(L8M(1)-L8M(steps+1))/sqrt(L8M(steps*1/2+1))
 S7 = k_R7
 S8 = k_R8
end


end

