% Calculates how Polarisation Sensitivity (PS) of R7 and R8 varies
% when changing the relative lengths of R7 and R8, while keeping 
% the total rhabdom length constant
%
% Author: fjh31@cam.ac.uk

l=100; % total length
step=2; % length increments in um
line_plot7={'b','b--','b-.','b:'}
line_plot8={'g','g--','g-.','g:'}

i = 1
for N = [1e3,1e4,1e5,1e6]
PS7a=[];
PS8a=[];
for l8=1:step:(l-1)
   [PS7, PS8] = PSR7R8(l-l8,l8,N);
   PS7a = [PS7a, PS7];
   PS8a = [PS8a, PS8];

    
end

figure(1)
plot(1:step:(l-1),PS7a,char(line_plot7(i)))
hold on
plot(1:step:(l-1),PS8a,char(line_plot8(i)))
i = 1+i
end

xlabel('R8 length fraction')
ylabel('PS')