% Calculates how SNR in R7 and R8, where noise includes photon shot
% noise but no internal noise, changes
% when changing the relative lengths of R7 and R8, while keeping 
% the total rhabdom length constant.
% Saturation can be considered or not, depending on the value of the
% variable:
account_for_saturation = 0;
% If 0, saturation is not considered (Fig5a). If 1, it is (Fig5b)
%
% Author: fjh31@cam.ac.uk


l=100; %total rhabdom length
step=2 %increment of length in um

N = [1e3 1e4 1e5 1e6] % light levels
N_txt = {'1e3','1e4','1e5','1e6'}
option_line={'-','--','-.',':'}


figure(1)
for i=1:length(N)

S7a=[];
S8a=[];
SNR7a=[];
SNR8a=[];
    

for l8=1:step:(l-1)
   l8
   [S7, S8, SNR7 ,SNR8] = SNR78(l-l8,l8,N(i),account_for_saturation);
   SNR7a = [SNR7a, SNR7]
   SNR8a = [SNR8a, SNR8]
    
end

plot((1:step:(l-1))/l,SNR7a,['b', option_line{i}],'LineWidth',2)
hold on
plot((1:step:(l-1))/l,SNR8a,['g', option_line{i}],'LineWidth',2)

end
xlabel('R8 length fraction')
ylabel('SNR')



