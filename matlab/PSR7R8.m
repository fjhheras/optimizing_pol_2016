function [PS7,PS8] = PSR7R8(l7,l8,N)

% Calculates PS in R7 and R8 of length l7 and l8
% when incoming photon rate is N and saturation is considered
% It calls "function_polarisation_stuff" to calculate the absorption 
% rates in R7 and R8 at 0 and pi/2 angle of
% polarisation.

set_values;
c=1; %polarisation coefficient of light, changed to 1 because we are calculating PS

steps=2;
angle_0=0; angle_1=pi/2;
%do_polarisation_stuff;
[ L7s,L8s,L7svar,L8svar,L7,L8,L7M,L8M ] = function_polarisation_stuff(N,c,l7,l8,k,D7,D8,deadtime,tau,angle_0,angle_1,steps);

%% Measure of PS saturated

PS7 = abs(L7s(1)/L7s(steps+1));
PS8 = abs(L8s(steps+1)/L8s(1));

%% Measure of PS not saturated (commented out)

%PS7 = abs(L7M(1)/L7M(steps+1));
%PS8 = abs(L8M(steps+1)/L8M(1));


end

