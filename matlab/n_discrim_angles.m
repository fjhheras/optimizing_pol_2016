function [DeltaS, DeltaS_nosat, DeltaQ, DeltaQ_nosat] = n_discrim_angles(l7,l8,N)

% Calculates the number of discriminable angles between 0 and pi/2
% and the opponent signalling range. 
% It calls "function_polarisation_stuff" to calculate the absorption 
% rates in R7 and R8 at 0 and pi/2 angle of polarisation. 
% Author: fjh31@cam.ac.uk

% Other Inputs:
% N - Incoming photon flux
% l7/l8 - length of photoreceptors, um

set_values;
steps=20; %Number of steps in distance calculation
verbosity=0;
angle_0=0; angle_1=pi/2;
[ L7s,L8s,L7svar,L8svar,L7,L8,L7M,L8M ] = function_polarisation_stuff(N,c,l7,l8,k,D7,D8,deadtime,tau,angle_0,angle_1,steps);

%% Saturated response
k_R7 = L7s(steps*1/2+1);
k_R8 = L8s(steps*1/2+1);
Delta_q_R7 = ( L7s(1:steps) - L7s(2:(steps+1)) ) ./ k_R7;
Delta_q_R8 = ( L8s(1:steps) - L8s(2:(steps+1)) ) ./ k_R8;
err = L7svar ./ k_R7./ k_R7 + L8svar ./ k_R8 ./ k_R8 + internal_noise_l7 + internal_noise_l8;
err = (err(1:steps) + err(2:(steps+1)))/2;
if (l7==l8 && verbosity)
  disp(sprintf('At %f''s ph/s, R7 catches %f photons and the fraction between the std of internal noise and photon noise is',N,k_R7/tau));
  disp(sqrt((internal_noise_l7 + internal_noise_l8)./(L7svar(steps*1/2+1) / k_R7/ k_R7 + L8svar(steps*1/2+1) /k_R8 / k_R8))) %shows how much internal noise SD is bigger than photon noise
end
Delta_S = abs(Delta_q_R7-Delta_q_R8)./sqrt(err);
DeltaS = sum(Delta_S);

%% Non-saturated response

k_R7 = L7M(steps*1/2+1);
k_R8 = L8M(steps*1/2+1);
Delta_q_R7_matrix = ( L7M(1:steps) - L7M(2:(steps+1)) )./ k_R7; 
Delta_q_R8_matrix = ( L8M(1:steps) - L8M(2:(steps+1)) ) ./ k_R8;
err = L7M ./ k_R7./ k_R7 + L8M ./ k_R8 ./ k_R8 + internal_noise_l7 + internal_noise_l8;
err = (err(1:steps) + err(2:(steps+1)))/2;

if (l7==l8 && verbosity)
  disp(sprintf('and if saturation is not considered:',N));
  disp(sqrt((internal_noise_l7 + internal_noise_l8)./(1/k_R7 + 1/k_R8))) %shows how much internal noise is bigger than photon noise
end

Delta_S_matrix = abs(Delta_q_R7_matrix-Delta_q_R8_matrix)./sqrt(err);
DeltaS_nosat = sum(Delta_S_matrix);



%% Measure of performance without noise

Q1=L7s(1)/L7s(steps*1/2+1)-L8s(1)/L8s(steps*1/2+1);
Q2=L7s(steps+1)/L7s(steps*1/2+1)-L8s(steps+1)/L8s(steps*1/2+1);

DeltaQ = abs(Q1-Q2);

Q1=L7M(1)/L7M(steps*1/2+1)-L8M(1)/L8M(steps*1/2+1);
Q2=L7M(steps+1)/L7M(steps*1/2+1)-L8M(steps+1)/L8M(steps*1/2+1);

DeltaQ_nosat = abs(Q1-Q2);

end

