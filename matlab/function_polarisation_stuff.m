function [ L7s,L8s,L7svar,L8svar,L7,L8,L7M,L8M ] = function_polarisation_stuff(N,c,l7,l8,k,D7,D8,deadtime,tau,angle_0,angle_1,steps)
%script calculating absorption in R7 and R8 for light polarised with degree
%of polarisation c and step+1 angles of polarisation between 0 and pi/2
%Used in many scripts of this folder
%Author: fjh31@cam.ac.uk

% Other inputs:
% N - Incoming photon flux
% c - degree of polarization
% l7/l8 - length of photoreceptors, um
% k - abs coeff for unpolarised light
% D7/8 - dichroic ratio of R7/8
% deadtime - microvillar dead time
% tau - integration time
length_of_section = 1; %length of section in the calculations

K7=2*(k-k/(D7+1)); k7=2*k/(D7+1); % abs coeff in the preferred and perpendicular directions
K8=2*(k-k/(D8+1)); k8=2*k/(D8+1); %


Nt7=length_of_section/250*9e4; Nt8=length_of_section/250*9e4;

A=[Fw(K7*l7) Fw(k8*l7); Fw(k8*l8+K7*l7)-Fw(K7*l7) Fw(K8*l8+k7*l7)-Fw(k7*l7)]; %matrix absorption, Warrant way
theta_array = linspace(angle_0, angle_1,steps+1);

L7 = zeros([1,steps+1]); L8 = zeros([1,steps+1]);
L7s = zeros([1,steps+1]); L8s = zeros([1,steps+1]);
L7svar = zeros([1,steps+1]); L8svar = zeros([1,steps+1]);
L7M = zeros([1,steps+1]); L8M = zeros([1,steps+1]);

ii7 = 1:l7/length_of_section; %index in R7
ii8 = (l7/length_of_section + 1):((l7+l8)/length_of_section); %index in R8

L=length_of_section*(1:((l7+l8)/length_of_section)); %Length down to section
kl1_a = [ones(1,l7/length_of_section)*K7 ones(1,l8/length_of_section)*k8]*length_of_section; % kl in each section
kl2_a = [ones(1,l7/length_of_section)*k7 ones(1,l8/length_of_section)*K8]*length_of_section;

sum1 = 0;
sum2 = 0;
for i=1:((l7+l8)/length_of_section)
    sum1 = sum1 + kl1_a(i);
    sum2 = sum2 + kl2_a(i);
    F1(i) = Fw(sum1); % Fraction of incident photons polarized parallel to R7 absorbed at or before section
    F2(i) = Fw(sum2); % idem for perpendicular to R7 preferred direction
end

Fd1 = conv([0 F1],[1 -1],'valid')  % Fraction of incident photons polarized parallel to R7 absorbed in section
Fd2 = conv([0 F2],[1 -1],'valid')  % Fraction of incident photons polarized perpendicular to R7 absorbed in section

for thetai=1:(steps+1)
    
    theta=theta_array(thetai);
    
    L1=N*((1-c)/2+c*cos(theta)*cos(theta)); % photons one polariz plane
    L2=N*((1-c)/2+c*sin(theta)*sin(theta)); % photons the other
    
    k1a=0; % this will be kappa=kl. Initialised at zero...
    k2a=0; %

    P = Fd1*L1 + Fd2*L2
    Ps = (1-exp(-P/Nt7*deadtime))./deadtime*Nt7; % Mean number photons saturation
    Psvar = exp(-P/Nt7*deadtime).*(1-exp(-P/Nt7*deadtime))./deadtime*Nt7; % Var number photons with saturation

    L7(thetai)=sum(P(ii7)*tau); L8(thetai)=sum(P(ii8)*tau); % This is photon absoption without considering saturation
    L7s(thetai)=sum(Ps(ii7)*tau); L8s(thetai)=sum(Ps(ii8)*tau); %Photon absoption considering saturation
    L7svar(thetai)=sum(Psvar(ii7)*tau); L8svar(thetai)=sum(Psvar(ii8)*tau); %Photon variance considering saturation
    a = A*[L1; L2];
    L7M(thetai)=a(1)*tau; % Again without considering saturation, but without
    L8M(thetai)=a(2)*tau; % sectioning photoreceptors and using matrix
end

end
