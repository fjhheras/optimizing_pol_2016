function [MI_sat, MI_nosat, PS_performance, PS_performance_no_sat] = distanceMI(l7,l8,N)
% distanceMI: calculates the mutual information between the opponent signal and
% the polarization angle. It assumes that degree of polarization is known.
% Input: 
% l7 - length of R7 in microns
% l8 - length of R8 in microns
% N - incident light flux in photons per second
% Output:
% MI_sat - mutual information considering saturation
% MI_nosat - mutual information not considering saturation
% PS_performance - a measure of the variation of the opponent signal

set_values;
minF = 1e-10; %Value of x below which xlogx is approximated as 0
steps=30;
angle_0=0; angle_1=pi/2;
angle_array=linspace(angle_0,angle_1,steps+1); %Array of the angles considered: steps+1 angles between angle_0 and angle_1

stepsq=60; qmax=1.5; 
q_array=linspace(-qmax,qmax,stepsq+1); %Array of the q considered: stepsq+1 between -qmax and qmax
[ L7s,L8s,L7svar,L8svar,L7,L8 ,L7M,L8M] = function_polarisation_stuff(N,c,l7,l8,k,D7,D8,deadtime,tau,angle_0,angle_1,steps);


%% Saturated response

k_R7 = L7s(steps*1/2 +1); %Value at pi/4 (assuming that angle0 = 0 and angle1 = pi/2)
k_R8 = L8s(steps*1/2 +1);

Delta_angle = pi/2/(steps+1);
Delta_q = 2*qmax/(stepsq+1);
F=zeros(steps+1,stepsq+1);
err = L7svar ./ k_R7./ k_R7 + L8svar ./ k_R8 ./ k_R8 + internal_noise_l7 + internal_noise_l8; %Noise in contrast space
for anglei=1:(steps+1)
   q = L7s(anglei)/k_R7 - L8s(anglei)/k_R8;
   prob = exp(-(q-q_array).*(q-q_array)/(2*err(anglei)));%/sqrt(err(anglei)*2*pi);  but normalized in next step
   F(anglei,:) = (2/pi) * prob/sum(prob)/Delta_q; %pdf of (q, angle)
end

f_q=sum(F)*Delta_angle; %marginal pdf f(q)
for anglei = 1:(steps+1)
    f_q_angle(anglei,:) = f_q*2/pi; %product of f(q) and f(angle)=2/pi(flat between 0 and pi/2)
end
sum(sum(F))*Delta_angle*Delta_q


%% Calculating MI_sat = sum(sum( F.*log(F./f_q_angle) ))*Delta_angle*Delta_q / log(2);
%  But in a convoluted way to avoid problems when calculating x log x at
%  low values of x

B = F./f_q_angle;
A = log(B(B > minF)).*F(B > minF);
MI_sat = sum(A)*Delta_angle*Delta_q / log(2);

%MI_sat = sum(sum( arrayfun(@xlog,F./f_q_angle).*f_q_angle ))*Delta_angle*Delta_q / log(2);


%% Non-saturated response

k_R7 = L7M(steps*1/2 +1);
k_R8 = L8M(steps*1/2 +1);
Delta_angle = pi/2/(steps+1);
Delta_q = 2*qmax/(stepsq+1);
F=zeros(steps+1,stepsq+1);
err = L7M ./ k_R7./ k_R7 + L8M ./ k_R8 ./ k_R8 + internal_noise_l7 + internal_noise_l8; %Noise in contrast space
for anglei=1:(steps+1)
   q = L7M(anglei)/k_R7 - L8M(anglei)/k_R8;
   prob = exp(-(q-q_array).*(q-q_array)/(2*err(anglei)));%/sqrt(err(anglei)*2*pi); but normalized in next step
   F(anglei,:) = (2/pi) * prob/sum(prob)/Delta_q;
end

f_q=sum(F)*Delta_angle; %marginal pdf f(q)
for anglei = 1:(steps+1)
    f_q_angle(anglei,:) = f_q*2/pi; %product of f(q) and f(angle)
end
sum(sum(F))*Delta_angle*Delta_q
minF=1e-16;

%% Calculating MI_nosat = sum(sum( F.*log(F./f_q_angle) ))*Delta_angle*Delta_q / log(2);
%  But in a convoluted way to avoid problems when calculating x log x at
%  low values of x
B = F./f_q_angle;
A = log(B(B > minF)).*F(B > minF);
MI_nosat = sum(A)*Delta_angle*Delta_q / log(2);

%MI_nosat = sum(sum( arrayfun(@xlog,F./f_q_angle).*f_q_angle ))*Delta_angle*Delta_q / log(2);

%% Measure of performance without noise

q1=L7s(1)/L7s(steps*1/2+1)-L8s(1)/L8s(steps*1/2+1);
q2=L7s(steps+1)/L7s(steps*1/2+1)-L8s(steps+1)/L8s(steps*1/2+1);

PS_performance = abs(q1-q2);

q1=L7M(1)/L7M(steps*1/2+1)-L8M(1)/L8M(steps*1/2+1);
q2=L7M(steps+1)/L7M(steps*1/2+1)-L8M(steps+1)/L8M(steps*1/2+1);

PS_performance_no_sat = abs(q1-q2);

end

