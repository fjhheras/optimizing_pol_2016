% Calculates how the number of discriminable polarization angles
% and the opponent signalling range DeltaQ changes
% with the total length of the rhabdomere, while keeping l8 = l7
% This is Fig 9 a,b,c,d if internal noise is 0 in "set_values.m"
% and Fig 9 a,b,e,f otherwise
% Author: fjh31@cam.ac.uk

l8range = 1:2:150 % Range of l8 values 
l8_reference = 50 % Normalization will be respect to the values at L = 100 um

colour_plot=['b','r','g','k']
photon_flux=[1e5,3e5,1e6,3e6]
N_light_levels=length(photon_flux)

for i=1:N_light_levels
  n_angle_a=[];
  n_angle_nosat_a=[];
  DeltaQ_a=[];
  DeltaQ_no_sat_a=[];
  
  [n_angle_reference ,n_angle_nosat_reference, DeltaQ_reference, DeltaQ_no_sat_reference] = n_discrim_angles(l8_reference,l8_reference,photon_flux(i));

  for l8=l8range
    2*l8
    [n_angle ,n_angle_nosat, DeltaQ, DeltaQ_no_sat] = n_discrim_angles(l8,l8,photon_flux(i));
    [PS7,PS8] = PSR7R8(l8,l8,photon_flux(i));
    
    n_angle_a=[n_angle_a, n_angle];
    n_angle_nosat_a=[n_angle_nosat_a, n_angle_nosat];
    DeltaQ_a=[DeltaQ_a, DeltaQ];
    DeltaQ_no_sat_a=[DeltaQ_no_sat_a, DeltaQ_no_sat];
  end
  
  figure(1)
  subplot(2,2,3)
  plot(l8range*2,n_angle_a,[colour_plot(i),'--'])
  hold on
  plot(l8range*2,n_angle_nosat_a,colour_plot(i))
    
  subplot(2,2,4)  
  plot(l8range*2,n_angle_a./n_angle_reference,[colour_plot(i),'--'])
  hold on
  plot(l8range*2,n_angle_nosat_a./n_angle_nosat_reference,colour_plot(i))
  
  subplot(2,2,1)
  plot(l8range*2,DeltaQ_a,[colour_plot(i),'--'])
  hold on
  plot(l8range*2,DeltaQ_no_sat_a,colour_plot(i))
  
  subplot(2,2,2)
  plot(l8range*2,DeltaQ_a./DeltaQ_reference,[colour_plot(i),'--'])
  hold on
  plot(l8range*2,DeltaQ_no_sat_a./DeltaQ_no_sat_reference,colour_plot(i))
  
 
end


figure(1)
subplot(2,2,3) 
xlim([0 300])
xlabel('L')
ylabel('Number of discriminable angles')

subplot(2,2,4) 
xlim([0 300])
xlabel('L')
ylabel('Number of discriminable angles (normalised)')

subplot(2,2,1) 
xlim([0 300])
xlabel('L')
ylabel('Opponent signalling range')

subplot(2,2,2) 
xlim([0 300])
xlabel('L')
ylabel('Opponent signalling range (normalised)')


