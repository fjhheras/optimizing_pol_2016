%Script that set values for various functions

k=0.0075; %absorption coeff 
c=.1; %polarisation coefficient of light 

D7=10; %dicroic ration of ph 7
D8=10; %dicroic ration of ph 8

tau=0.09; %integration time
deadtime=0.03;
internal_noise_l7 = 0.00005/tau; 
internal_noise_l8 = 0.00005/tau;