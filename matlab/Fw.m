function [ Fwo ] = Fw( kl )
%FW Absorption of sky light by a photoreceptor with absorption peaking at
%335 nm. Uncomment as necessary. 

%% Warrant and Nilsson (1997) approximation
%h=0.35;
%m=0.96; %both points are extrapolations
%m=1;
%Fwo=power(kl,m)./(power(10,h*m)+power(kl,m)); %Warrant

%% Exponential absorption
%Fwo=1-exp(-kl);

%% Function calculated in the paper
Fwo=(1-exp(-kl)).*polyval([-0.00291346  0.05512361  0.4697838],kl);


end

